import pybithumb


class BithumbApi:

    def __init__(self):
        self.bithumb = pybithumb.Bithumb("587ba6d9260653eeaaa947f94bde2fc2", "72323c4da96de1ffcf9a97c48fb2f413")

    def getTargetPrice(self, ticker):
        df = pybithumb.get_ohlcv(ticker)
        yesterday = df.iloc[-2]
        todayOpen = yesterday['close']
        yesterdayHigh = yesterday['high']
        yesterdayLow = yesterday['low']
        target = todayOpen + (yesterdayHigh - yesterdayLow) * 0.5
        return target

    def getBalance(self, ticker):
        balance = self.bithumb.get_balance(ticker)
        print(balance)
        return balance

    def buyLimit(self, ticker, price, amount):
        order = self.bithumb.buy_limit_order(ticker, price, amount)
        print(order)

    def buyMarket(self, ticker, amount):
        order = self.bithumb.buy_market_order(ticker, amount)
        print(order)

    def getTickers(self):
        return self.bithumb.get_tickers()
