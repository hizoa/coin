from PyQt5.QtCore import *
import pybithumb
import time
import datetime
from api.bithumbApi import *


class Worker(QThread):
    bithumbApi = BithumbApi()
    tickerList = bithumbApi.getTickers()
    targetPriceDic = {}
    buyList = []
    now = datetime.datetime.now()
    mid = datetime.datetime(now.year, now.month, now.day) + datetime.timedelta(1)
    stopFlag = False

    def run(self):
        while self.stopFlag is False:
            now = datetime.datetime.now()
            mid = datetime.datetime(now.year, now.month, now.day) + datetime.timedelta(1)
            if mid < now < mid + datetime.timedelta(0, 10):
                print(str(now) + "정각입니다")
                self.buyList.clear()
                self.targetPriceDic.clear()
                for ticker in self.tickerList:
                    self.targetPriceDic[ticker] = self.bithumbApi.getTargetPrice(ticker)

            if self.targetPriceDic.__len__() == 0:
                self.targetPriceDic.clear()
                for ticker in self.tickerList:
                    target = self.bithumbApi.getTargetPrice(ticker)
                    self.targetPriceDic[ticker] = target
                    print(ticker + str(target))

            for ticker in self.tickerList:
                currentPrice = pybithumb.get_current_price(ticker)
                if ticker in self.targetPriceDic.keys():
                    targetPrice = self.targetPriceDic[ticker]
                    if currentPrice > targetPrice:
                        #krw = self.bithumbApi.getBalance(ticker)[2]
                        if self.bithumbApi.getBalance(ticker)[0] == 0:
                            krw = 2000
                            orderBook = pybithumb.get_orderbook(ticker)
                            sellPrice = orderBook['asks'][0]['price']
                            unit = krw/float(sellPrice)
                            self.bithumbApi.buyMarket(ticker, unit)
                            self.buyList.append(ticker)
                            print(currentPrice)

            df = pybithumb.get_ohlcv("BTC")
            datetime.timedelta(0, 0, 10)
            print(df.tail())
            time.sleep(5)









