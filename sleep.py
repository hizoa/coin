import sys
from PyQt5.QtWidgets import *
from PyQt5 import uic
from PyQt5.QtCore import *
from auto.variance import *
import pybithumb

formClass = uic.loadUiType("hangang.ui")[0]


class MyWindow(QMainWindow, formClass):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.worker = Worker()
        self.worker.start()


app = QApplication(sys.argv)
window = MyWindow()
window.setWindowTitle('한강뷰')
window.show()
app.exec_()
